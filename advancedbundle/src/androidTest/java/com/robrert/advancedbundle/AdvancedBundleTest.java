package com.robrert.advancedbundle;

import android.os.Bundle;
import android.test.InstrumentationTestCase;

import java.util.Locale;

/**
 * Created by Robert on 2016-06-09.
 */


public class AdvancedBundleTest extends InstrumentationTestCase {

    public void testCreateBundle() throws Exception {
        AdvancedBundle myBundle = AdvancedBundle.createBundle();
        myBundle.put("MyObject", Locale.US);
        Bundle simple = myBundle.getSimpleBundle();
        AdvancedBundle restored = AdvancedBundle.restore(simple);
        assertEquals(Locale.US, restored.get("MyObject"));
    }


    public void testSaveState() throws Exception {
        TestClass cls = new TestClass();
        cls.lcl = Locale.CANADA;
        cls.myInt = 69;
        cls.int2 = 10;
        Bundle bundle = new Bundle();
        AdvancedBundle.saveState(cls, bundle);
        TestClass newClass = new TestClass();
        AdvancedBundle.restoreState(newClass, bundle);
        assertEquals(cls.lcl, newClass.lcl);
        assertEquals(cls.myInt, newClass.myInt);
        assertNotSame(cls.int2, newClass.int2);
    }

    public void testThreadSafety() throws Exception {
        final String testString = "testObject";
        Thread thread = null;
        for (int i = 0; i < 50; i++) {
            final int delay = 25 * i;
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < 1000; j++) {
                        Object testObject = new Object();
                        AdvancedBundle advancedBundle = AdvancedBundle.createBundle();
                        advancedBundle.put(testString, testObject);
                        String validString = "TestStringData" + j;
                        advancedBundle.put("ThreadString", validString);
                        Bundle simple = advancedBundle.getSimpleBundle();
                        try {
                            Thread.sleep(1, delay);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        try {
                            AdvancedBundle restored = AdvancedBundle.restore(simple);
                            assertEquals(testObject, restored.get(testString));
                            assertEquals(validString, advancedBundle.get("ThreadString"));
                        } catch (BundleRestoredException e) {
                            System.out.println(Thread.currentThread().getName() + " j: " + j);
                            System.out.println(simple.getLong("AdvancedBundleObjectId"));
                            throw e;
                        }
                    }
                }
            });
            thread.setName("Test thread - " + i);
            thread.start();
        }
        thread.join();
    }

    public class TestClass {

        @SaveState
        Locale lcl;

        @SaveState
        Integer myInt;

        int int2;

    }
}