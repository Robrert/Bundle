package com.robrert.advancedbundle;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * AdvancedBundle is object that can store any type of objects. It's instances are stored in HashMap
 *
 * <p>It provides {@link AdvancedBundle#getSimpleBundle()} method that returns {@link Bundle} object
 * witch can be passed into {@link Intent} or another {@code Bundle}.<br>
 * This simple bundle is also used later to retrieve {@code AdvancedBundle} instance using
 * {@link AdvancedBundle#restore(Bundle)}
 * </p>
 *
 * <p>Useful class to pass objects that are not {@link java.io.Serializable} or {@link android.os.Parcelable}</p>
 *
 *
 * <p>AdvancedBundle also provides functions to automatic saving and restoring instance state of objects.
 * To do that use {@link AdvancedBundle#saveState(Object, Bundle)} to save state of object.<br>
 * Every field's annotated with {@link SaveState} value in passed object will be saved in {@code AdvancedBundle} instance
 * and later restored via {@link AdvancedBundle#restoreState(Object, Bundle)}</p>
 *
 * @author Robert Pakowski
 * @version 1.0
 */
public class AdvancedBundle {

    private static final String BUNDLE_KEY = "AdvancedBundleObjectId";

    private static final ConcurrentHashMap<Long, AdvancedBundle> advancedBundles = new ConcurrentHashMap<>();
    private static final HashMap<Class<?>, Fields> cache = new HashMap<>();
    private static final AtomicLong ids = new AtomicLong(1);


    /**
     * Creates {@code AdvancedBundle} object. After using this method,
     * every object You put into this AdvancedBundle,
     * You will be able to restore when necessary.
     *
     * @return {@code AdvancedBundle} object to store data
     */
    public static AdvancedBundle createBundle() {
        long bundleId = generateId();
        AdvancedBundle advancedBundle = new AdvancedBundle(bundleId);
        synchronized (advancedBundles) {
            advancedBundles.put(bundleId, advancedBundle);
        }
        return advancedBundle;
    }

    @NonNull
    private static Long generateId() {
        synchronized (ids) {
            long bundleId;
            bundleId = ids.getAndIncrement();
            if(bundleId == 0) {
                bundleId = ids.getAndIncrement();
            }
            return bundleId;
        }
    }


    /**
     * Creates {@code AdvancedBundle} objects, and puts into it all values stored
     * in fields of {@code o} annotated with {@code SaveState}
     *
     * @param o object witch state You want to save
     * @param saveState bundle that will contain saved state
     * @throws BundleException when error occurred
     */
    public static void saveState(@NonNull Object o, @NonNull Bundle saveState) throws BundleException, IllegalBundleStateException {
        if(saveState.get(BUNDLE_KEY + o.getClass().getName()) != null) {
            throw new BundleKeyUsedException("Bundle" + saveState.toString() +
                    "already contains Object with key used to store bundle: " + BUNDLE_KEY + o.getClass().getName());
        }
        long bundleId = generateId();
        AdvancedBundle advancedBundle = new AdvancedBundle(bundleId);
        Class<?> clazz = o.getClass();
        try {
            while (clazz != null) {
                for (Field field : getCachedFields(clazz)) {
                    boolean access = field.isAccessible();
                    field.setAccessible(true);
                    advancedBundle.put(field.getName(), field.get(o));
                    field.setAccessible(access);
                }
                clazz = getSuperClass(clazz);
            }
        } catch (Exception e) {
            throw new SaveStateException("Unable to save state", e);
        }
        saveState.putBundle(BUNDLE_KEY + o.getClass().getName(), advancedBundle.getSimpleBundle());
        synchronized (advancedBundles) {
            advancedBundles.put(bundleId, advancedBundle);
        }
    }

    @Nullable
    private static Class<?> getSuperClass(Class<?> clazz) {
        Class<?> superClass = clazz.getSuperclass();
        if(superClass != null) {
            String name = clazz.getName();
            if(name.startsWith("java.") || name.startsWith("javax.") || name.startsWith("android.")) {
                if(superClass.isAnnotationPresent(Include.class)) {
                    return superClass;
                } else {
                    return null;
                }
            }
        }
        return superClass;
    }

    private static Fields getCachedFields(Class<?> clazz) {
        Fields fields;
        synchronized (cache) {
            fields = cache.get(clazz);
            if(fields == null) {
                fields = new Fields();
                cache.put(clazz, fields);
            }
        }
        //noinspection SynchronizationOnLocalVariableOrMethodParameter
        synchronized (fields) {
            if (fields.firstUse()) {
                for (Field field : clazz.getDeclaredFields()) {
                    if (isInstanceField(field) && field.isAnnotationPresent(SaveState.class)) {
                        fields.add(field);
                    }
                }
            }
        }
        return fields;
    }

    private static boolean isInstanceField(Field field) {
        return (field.getModifiers() & Modifier.STATIC) == 0;
    }

    @NonNull
    public static AdvancedBundle restore(@NonNull Bundle bundle) {
        long bundleId = bundle.getLong(BUNDLE_KEY, 0);
        if(bundleId != 0) {
            AdvancedBundle advancedBundle = advancedBundles.remove(bundleId);
            if(advancedBundle == null) {
                throw new BundleRestoredException("AdvancedBundle associated with " + bundle.toString() + " have already been restored");
            } else {
                return advancedBundle;
            }
        } else {
            throw new IllegalArgumentException("Bundle: " + bundle.toString() + " does not contain valid data to restore AdvancedBundle instance");
        }
    }


    /**
     * @param o Object witch state You want to restore
     * @param restoredState bundle containing data to restore state of object
     * @throws RestoreStateException when error occurred
     */
    @SuppressWarnings("ConstantConditions")
    public static void restoreState(@NonNull Object o, @NonNull Bundle restoredState) throws RestoreStateException {
        AdvancedBundle advancedBundle = null;
        try {
            Class<?> clazz = o.getClass();
            advancedBundle = restore(restoredState.getBundle(BUNDLE_KEY + clazz.getName()));
            while (clazz != null) {
                for (Field field : getCachedFields(o.getClass())) {
                    boolean access = field.isAccessible();
                    field.setAccessible(true);
                    field.set(o, advancedBundle.data.get(field.getName()));
                    field.setAccessible(access);
                }
                clazz = getSuperClass(clazz);
            }
        } catch (Exception e) {
            throw new RestoreStateException("Unable to restore state", e, advancedBundle);
        }
    }

    private AdvancedBundle(long bundleId) {
        simpleBundle.putLong(BUNDLE_KEY, bundleId);
    }

    private final Bundle simpleBundle = new Bundle();

    private final HashMap<String, Object> data = new HashMap<>();


    /**
     * @param key {@code String} that may be used later to retrieve passed object
     * @param o Object You want to store
     * @return {@code this AdvancedBundle} object allowing execution chains
     */
    public AdvancedBundle put(@NonNull String key, Object o) {
        data.put(key, o);
        return this;
    }


    /**
     * @param key String associated with object to get
     * @return Object associated with {@code key} or null, when there is no such object
     */
    @Nullable
    public Object get(@NonNull String key) {
        return data.get(key);
    }

    public boolean contains(@NonNull String key) {
        return data.containsKey(key);
    }

    @NonNull
    public Bundle getSimpleBundle() {
        return simpleBundle;
    }

}
