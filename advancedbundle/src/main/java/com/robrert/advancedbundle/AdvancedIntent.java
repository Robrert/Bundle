package com.robrert.advancedbundle;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;

/**
 * Subclass of {@link Intent} that provides functions to handle putting {@link AdvancedBundle} object from Intent
 * Because of android mechanisms Intent object received in Activity/Service is pure Intent,
 * so restoring AdvancedBundle must be handled by static function
 * {@link AdvancedIntent#restoreAdvancedBundleExtra(Intent, String)}
 *
 * @author Robert Pakowski
 * @version 1.0
 */
public class AdvancedIntent extends Intent {

    public AdvancedIntent() {
        super();
    }

    public AdvancedIntent(Intent o) {
        super(o);
    }

    public AdvancedIntent(String action) {
        super(action);
    }

    public AdvancedIntent(String action, Uri uri) {
        super(action, uri);
    }

    public AdvancedIntent(Context packageContext, Class<?> cls) {
        super(packageContext, cls);
    }

    public AdvancedIntent(String action, Uri uri, Context packageContext, Class<?> cls) {
        super(action, uri, packageContext, cls);
    }

    private AdvancedIntent(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<AdvancedIntent> CREATOR = new Creator<AdvancedIntent>() {
        @Override
        public AdvancedIntent createFromParcel(Parcel source) {
            return new AdvancedIntent(source);
        }

        @Override
        public AdvancedIntent[] newArray(int size) {
            return new AdvancedIntent[0];
        }
    };

    public void putExtra(String key, AdvancedBundle advancedBundle) {
        Bundle simpleBundle = advancedBundle.getSimpleBundle();
        putExtra(key, simpleBundle);
    }

    public static AdvancedBundle restoreAdvancedBundleExtra(Intent intent, String key) {
        Bundle simpleBundle = intent.getBundleExtra(key);
        if(simpleBundle != null) {
            return AdvancedBundle.restore(simpleBundle);
        } else {
            return null;
        }
    }
}
