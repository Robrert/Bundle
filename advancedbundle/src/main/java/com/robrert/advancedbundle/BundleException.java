package com.robrert.advancedbundle;

import android.os.Bundle;

/**
 * Basic exception. It's subclasses will be thrown if any problem occur while saving or
 * restoring instance state of object
 *
 * @see AdvancedBundle#saveState(Object, Bundle)
 * @see AdvancedBundle#restoreState(Object, Bundle)
 *
 * @author Robert Pakowski
 * @version 1.0
 */
public abstract class BundleException extends Exception {

    public BundleException(String s, Exception e) {
        super(s, e);
    }
}
