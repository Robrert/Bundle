package com.robrert.advancedbundle;

import android.os.Bundle;

/**
 * Exception that is thrown when attempting to save object's state with {@code String key} that is already
 * used for different object in {@code Bundle} passed to {@link AdvancedBundle#saveState(Object, Bundle)}
 *
 * @see AdvancedBundle#saveState(Object, Bundle)
 *
 * @author Robert Pakowski
 * @version 1.0
 */
public class BundleKeyUsedException extends IllegalBundleStateException {
    public BundleKeyUsedException(String s) {
        super(s);
    }
}
