package com.robrert.advancedbundle;

import android.os.Bundle;

/**
 * Exception that is thrown when attempting to restore AdvancedBundle object after it was restored
 *
 * @see AdvancedBundle#saveState(Object, Bundle)
 *
 * @author Robert Pakowski
 * @version 1.0
 */
public class BundleRestoredException extends IllegalBundleStateException {

    public BundleRestoredException(String s) {
        super(s);
    }
}
