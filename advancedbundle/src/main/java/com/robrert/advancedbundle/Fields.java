package com.robrert.advancedbundle;

import java.lang.reflect.Field;
import java.util.Vector;

/**
 * Created by Robert on 2016-06-09.
 */
class Fields extends Vector<Field> {

    private boolean isNew = true;

    boolean firstUse() {
        if(modCount == 0) {
            if (isNew) {
                isNew = false;
                return true;
            } else {
                return false;
            }
        } else {
            isNew = false;
            return false;
        }
    }
}
