package com.robrert.advancedbundle;

import android.os.Bundle;

/**
 * Base extension of IllegalStateException that will be used in this library and thrown when
 *
 * @see AdvancedBundle#saveState(Object, Bundle)
 *
 * @author Robert Pakowski
 * @version 1.0
 */
public class IllegalBundleStateException extends IllegalStateException {
    public IllegalBundleStateException(String s) {

    }
}
