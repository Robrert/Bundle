package com.robrert.advancedbundle;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * By default every class in package "java.*", "javax.*" and "android.*" and it's super classes
 * will by skipped while searching for fields annotated with {@code @SaveState}, but if class
 * will be annotated with {@code @Include} the class will aso be searched
 *
 * @see SaveState
 *
 * @author Robert Pakowski
 * @version 1.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Include {
}
