package com.robrert.advancedbundle;

import android.os.Bundle;

/**
 * Exception that is thrown when problem occurred while restoring instance state
 *
 * @see AdvancedBundle#saveState(Object, Bundle)
 *
 * @author Robert Pakowski
 * @version 1.0
 */
public class RestoreStateException extends BundleException {

    private final AdvancedBundle bundle;

    public RestoreStateException(String s, Exception e, AdvancedBundle advancedBundle) {
        super(s, e);
        this.bundle = advancedBundle;
    }


    /**
     * @return AdvancedBundle object witch contains saved state that has been being restored
     */
    public AdvancedBundle getBundle() {
        return bundle;
    }
}
