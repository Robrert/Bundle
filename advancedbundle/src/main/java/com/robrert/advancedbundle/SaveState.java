package com.robrert.advancedbundle;

import android.os.Bundle;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that field annotated with this will be used to save state in
 * {@code AdvancedBundle.saveState(Object, Bundle)}
 * and then it's value will be restored in
 * {@code AdvancedBundle.restoreState(Object, Bundle) }
 *
 * @see AdvancedBundle
 * @see AdvancedBundle#saveState(Object, Bundle)
 * @see AdvancedBundle#restoreState(Object, Bundle)
 *
 * @author Robert Pakowski
 * @version 1.0
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SaveState {
}
