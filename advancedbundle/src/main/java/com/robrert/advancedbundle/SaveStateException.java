package com.robrert.advancedbundle;

import android.os.Bundle;

/**
 * Exception that is thrown when problem occurred while saving instance state
 *
 * @see AdvancedBundle#saveState(Object, Bundle)
 *
 * @author Robert Pakowski
 * @version 1.0
 */
public class SaveStateException extends BundleException {
    public SaveStateException(String s, Exception e) {
        super(s, e);
    }
}
